import logging
import sys

logging.basicConfig(level=logging.INFO, stream=sys.stdout)
logger = logging.getLogger(__name__)


# In a more production implementation, you'd probably want to upload the file directly to object store (
# openstack-swift/aws-s3). Then create some database reference to the file and state of the word count "job".
# Finally publish an event to whichever bus (rabbitmq, kafka, beanstalkd, ect) and process the event with a
# separately scalable async worker consuming off that bus. That wordcount job consumer would ideally stream the
# object and parse it word by word to avoid OOM and give deterministic performance characteristic. This endpoint
# would return a link to poll on for job status and ultimately the final word count or a failure message.

async def get_word_count_v1(payload: str) -> int:
    return len(payload.split())


async def get_word_count_v2():
    # TODO
    pass
