import pytest

from .word_count import get_word_count_v1

# Setting up testing framework (pytest) just so it's ready when we need it


@pytest.mark.asyncio
async def test_word_count(event_loop):
    test_str = 'cat dog matt hat crispy'
    assert await get_word_count_v1(test_str) == len(test_str.split())
