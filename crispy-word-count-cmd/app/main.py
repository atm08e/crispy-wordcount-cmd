import logging
import sys

import aiohttp
from aiohttp import web

from .settings import Settings
from .word_count import get_word_count_v1


logging.basicConfig(level=logging.INFO, stream=sys.stdout)
logger = logging.getLogger(__name__)


async def startup(app: web.Application) -> None:
    logger.info('Trying to start {} service!'.format(app['settings'].name))
    # Where you'd setup your database connection pool.
    # app['db'] = Super Awesome Database.connect()

    # Where you'd setup your bus (rabbitmq, kafka, ect) connection pool.
    # app['amqp'] = Super Awesome Bus.connect()


async def cleanup(app: web.Application) -> None:
    logger.info('Trying to gracefully shut down {} service...'.format(app['settings'].name))
    # Where you'd cleanup database and bus connection pools gracefully.
    # await app['db'].wait_and_close_connection()
    # await app['amqp].wait_and_close_connection()
    logger.info('Shut down {} service!'.format(app['settings'].name))


async def handle_word_count_v1(request: aiohttp.request) -> web.Response:
    # As is, this is a very simple and naive implementation for the sake of the demo/interview. It may suffer from
    # timeout and OOM issues with very large files. There is also not a lot of error checking. eg. You may also want to
    # enforce Content-Length limits via headers.
    payload = await request.text()
    #
    word_count = await get_word_count_v1(payload=payload)
    #
    return web.Response(text='{}'.format(word_count))


async def create_app() -> web.Application:
    app = web.Application()
    settings = Settings()
    app.update(
        settings=settings,
    )

    app.on_startup.append(startup)
    app.on_cleanup.append(cleanup)

    # Maybe return an html page where the file could be selected and uploaded via browser
    app.router.add_get('/', lambda req: web.Response(text=settings.name))

    # TODO The health of the service to be computed and returned. eg. if we had a database or bus connection,
    #  the health of that resource should be returned in health.
    app.router.add_get('/health', lambda req: web.json_response({'status': 'UP'}))

    app.router.add_put('/v1/word_count', handle_word_count_v1)

    # TODO real production implementation, as noted in word_count.py
    # app.router.add_put('/v2/wordcount', handle_wordcount)
    return app
