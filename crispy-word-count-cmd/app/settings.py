from pydantic import BaseSettings


class Settings(BaseSettings):
    # Any settings go here. use ENVIRONMENTAL variable to get a grip on secrets or overridable settings
    name = 'crispy-word-count-cmd'
