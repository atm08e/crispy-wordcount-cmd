# crispy-word-count-cmd
Crisp take home test word count service entry.
### Quick Start
To stand up the service with one command.
```bash
docker-compose up --build
```
To use/test the service with one command, feed it a file you wish to get a word count on. Word count will be returned as plane text.
```bash
curl -T ./README.md http://localhost:8080/v1/word_count
```
Verify word count is correct by using standard built in wc.
```bash
wc -w README.md
```
### Setup local environment
```bash
python3 -m venv venv
source venv/bin/activate
(venv) pip install -r requirements.txt
# adev runserver will automatically reload the webserver on local code changes
(venv) adev runserver crispy-word-count-cmd/app
```
### Run the tests locally
```bash
(venv) pytest
```
