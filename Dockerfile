FROM python:3.9-alpine
USER root
RUN apk add --no-cache --virtual .build-deps \
  gcc \
  g++ \
  make \
  musl-dev \
  libffi-dev \
  openssl-dev \
  python3-dev
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY crispy-word-count-cmd /opt
EXPOSE 8080
WORKDIR /opt
USER nobody
CMD ["gunicorn", "app.main:create_app", "--bind", "0.0.0.0:8080", "--worker-class", "aiohttp.GunicornWebWorker"]
